package com.simen.invoker;

import java.lang.reflect.Field;

/**
 * @author Created by Simen.
 * @date 创建日期 2017/8/2 16:06
 * @modify 修改者 Simen
 */
public class ClassUtils {

    /**
     * 将对象中所有的变量转为字符串
     *
     * @param object 对象
     * @return 返回字符串
     */
    public static String fieldsToString(Object object) {
        Class classType = object.getClass();
        Field[] fields = classType.getDeclaredFields();

        StringBuilder builder = new StringBuilder(classType.getName() + "{");
        if (fields != null) {
            for (int index = 0; index < fields.length; index++) {
                Field field = fields[index];

                builder.append(field.getName() + "=\'");
                if (!field.isAccessible()) {
                    field.setAccessible(true);
                }

                try {
                    builder.append(field.get(object) + "\'");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                if (index + 1 != fields.length) {
                    builder.append(",");
                }
            }
        }
        builder.append("}");
        return builder.toString();
    }

}
