# InvokeHelper

#### 项目介绍
Java反射Utils类

#### 使用说明

```xml
<dependency>
  <groupId>com.simen.invoker</groupId>
  <artifactId>Invoker</artifactId>
  <version>1.2.0</version>
  <type>pom</type>
</dependency>
```

```groovy
compile 'com.simen.invoker:Invoker:1.2.0'
```